#include "stdafx.h"
#include "Jakobian2D.h"



Jakobian2D::Jakobian2D(JakobianTablice *Jtab, double X[], double Y[])
{
	x = X;
	y = Y;
	for (int i = 0; i < 4; i++)
	{
		Xp[i] = Jtab->N[i][0]*x[0]+ Jtab->N[i][1] * x[1] + Jtab->N[i][2] * x[2] + Jtab->N[i][3] * x[3];
		Yp[i] = Jtab->N[i][0] *y[0] + Jtab->N[i][1] * y[1] + Jtab->N[i][2] * y[2] + Jtab->N[i][3] * y[3];
	//----------------------------------------------------------------------------------------------------
		J[0][i] = Jtab->dN_dksi[0][i] * x[0] + Jtab->dN_dksi[1][i] * x[1] + Jtab->dN_dksi[2][i] * x[2] + Jtab->dN_dksi[3][i] * x[3];
		J[1][i] = Jtab->dN_dksi[0][i] * y[0] + Jtab->dN_dksi[1][i] * y[1] + Jtab->dN_dksi[2][i] * y[2] + Jtab->dN_dksi[3][i] * y[3];
		J[2][i] = Jtab->dN_deta[0][i] * x[0] + Jtab->dN_deta[1][i] * x[1] + Jtab->dN_deta[2][i] * x[2] + Jtab->dN_deta[3][i] * x[3];
		J[3][i] = Jtab->dN_deta[0][i] * y[0] + Jtab->dN_deta[1][i] * y[1] + Jtab->dN_deta[2][i] * y[2] + Jtab->dN_deta[3][i] * y[3];
	//--------------------------------------------------------------------------------------------------------------------------	
		DetJ[i] = J[0][i] * J[3][i] - J[1][i] * J[2][i];
	//---------------------------------------------------
		Jakobian[0][i] = J[3][i] / DetJ[i];
		Jakobian[1][i] = -J[1][i] / DetJ[i];
		Jakobian[2][i] = -J[2][i] / DetJ[i];
		Jakobian[3][i] = J[0][i] / DetJ[i];
	}
}

double Jakobian2D::len(int a)
{
	return sqrt(pow(x[a%4]-x[(a+1)%4],2)+ pow(y[a%4] - y[(a + 1) % 4], 2));
}

void Jakobian2D::wypisz()
{
	cout << "Xp:" << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << Xp[i] << "  ";
	}
	cout << endl << "Yp:" << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << Yp[i] << "  ";
	}
	cout << endl << "X:" << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << x[i] << "  ";
	}
	cout << endl << "Y:" << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << y[i] << "  ";
	}
	cout << endl << "J:" << endl;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << J[i][j] << "  ";
		}
		cout << endl;
	}
	cout << endl << "DetJ:" << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << DetJ[i] << "  ";
	}
	cout << endl << "Jakobian:" << endl;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << Jakobian[i][j] << "  ";
		}
		cout << endl;
	}
}

Jakobian2D::~Jakobian2D()
{
}
