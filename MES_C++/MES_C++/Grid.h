#pragma once
#include "Node.h"
#include "Element.h"
#include"JakobianTablice.h"
#include"Jakobian2D.h"
#include"Matrix_H.h"
#include"Matrix_C.h"
#include"Wektor_P.h"
#include<iomanip>
using namespace std;
class Grid
{
	Node *node;
	Element *elements;
	int nH, nL, nh, nE;
	double H, L,T;
	int nw;
	double **Global_H ;
	double **Global_C ;
	double *Global_P ;
	double *Vector_P;
	double **GlobalHP;

public:
	void GridCreate(double temperatura);
	void wypisz();
	void calculateSteps(double ambient_temperature,double  alfa,double  specific_heat, double conductivity,double  density,double simulation_step_time,double simulation_time);
	int *board(Node a, Node b, Node c, Node d);
	double *getXforElement(Element e);
	double *getYforElement(Element e);
	void setGlobalHP(double simulation_step_time);
	void updateP(double simulation_step_time);
	void print();
	void printTemp();
	double maxT();
	double minT();
	bool Gauss(int n);
	Grid(double H, double L, int nH, int nL);

	~Grid();
};

