#pragma once
#include"JakobianTablice.h"
#include"Jakobian2D.h"
class Wektor_P
{
	double vecP[4][4];

public:
	double P[4] = { 0,0,0,0 };
	
	Wektor_P(Jakobian2D * J2D, JakobianTablice *Jtab, double  alfa, double ambient_temperature, int *border);
	~Wektor_P();
};

