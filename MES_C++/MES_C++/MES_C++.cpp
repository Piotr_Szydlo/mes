// MES_C++.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include"Grid.h"
#include<fstream>
using namespace std;


int main()
{

	double initial_temperature;
	int simulation_time;
	int simulation_step_time;
	double ambient_temperature;
	double alfa;
	double H;
	double L;
	int N_H;
	int N_L;
	double specific_heat;
	double conductivity;
	double density;

	ifstream data("InitialData.txt");
	string line;
	if (data.is_open())
	{
		while (!data.eof())
		{
			data >> initial_temperature;
			data >> simulation_time;
			data >> simulation_step_time;
			data >> ambient_temperature;
			data >> alfa;
			data >> H;
			data >> L;
			data >> N_H;
			data >> N_L;
			data >> specific_heat;
			data >> conductivity;
			data >> density;
		}
		data.close();
	}
	cout<<"Initial temp: "<< initial_temperature<<"Symulation time: "<< simulation_time<<"Step: "<< simulation_step_time<<"Temp Otocz: "<<ambient_temperature
	<<"Alfa: "<< alfa<<"H: "<< H<<"L: "<<L<<"N_H: "<<N_H<<"N_L: "<<N_L<<"CW: "<<specific_heat<< "conductivity: "<<conductivity<<"Gest: "<<density<<endl;
	Grid *grid = new Grid(H, L, N_H, N_L);
	grid->GridCreate(initial_temperature);

	grid->calculateSteps(ambient_temperature, alfa, specific_heat, conductivity, density, simulation_step_time, simulation_time);

	
    return 0;
}

