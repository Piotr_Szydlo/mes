#include "stdafx.h"
#include "Matrix_H.h"




void Matrix_H::wypisz()
{
	cout << "H:" << endl;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << H[i][j] << "  ";
		}
		cout << endl;
	}
}

Matrix_H::Matrix_H(Jakobian2D * J2D, JakobianTablice * Jtab,double conductivity,double alfa,int *border )
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			dN_dx[i][j] = J2D->Jakobian[0][i] * Jtab->dN_dksi[j][i] + J2D->Jakobian[1][i] * Jtab->dN_deta[j][i];
			dN_dy[i][j] = J2D->Jakobian[2][i] * Jtab->dN_dksi[j][i] + J2D->Jakobian[3][i] * Jtab->dN_deta[j][i];
			sumConvection[i][j] = 0;


		}
	}
	for(int k=0;k<4;k++)
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (border[k] == 1)
				{
					convectionTab[0][i][j] = Jtab->NB[k][0][j] * Jtab->NB[k][0][i] * alfa;
					convectionTab[1][i][j] = Jtab->NB[k][1][j] * Jtab->NB[k][1][i] * alfa;
					sumConvection[i][j] += (convectionTab[0][i][j] + convectionTab[1][i][j])*(J2D->len(k) / 2);

				}
				dN_dx_T[k][i][j] = dN_dx[k][i] * dN_dx[k][j]*J2D->DetJ[k];
				dN_dy_T[k][i][j] = dN_dy[k][i] * dN_dy[k][j]* J2D->DetJ[k];
				Sum_K[k][i][j] = (dN_dx_T[k][i][j] + dN_dy_T[k][i][j])*conductivity;
			}

		}
			

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			H[i][j] = Sum_K[0][i][j] + Sum_K[1][i][j] + Sum_K[2][i][j] + Sum_K[3][i][j]+sumConvection[i][j];
		}
	}
	

}

Matrix_H::~Matrix_H()
{
}
