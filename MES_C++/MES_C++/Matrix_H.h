#pragma once
#include"JakobianTablice.h"
#include"Jakobian2D.h"
class Matrix_H
{
	
	double dN_dx[4][4];
	double dN_dy[4][4];
	double dN_dx_T[4][4][4];
	double dN_dy_T[4][4][4];
	double Sum_K[4][4][4];
	double convectionTab[2][4][4];
	double sumConvection[4][4];

public:
	double H[4][4];

	void wypisz();

	Matrix_H(Jakobian2D *J2D,JakobianTablice *Jtab,double conductivity,double alfa, int *border);
	~Matrix_H();
};

