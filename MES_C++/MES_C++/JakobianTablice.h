#pragma once
#include<cmath>
#include<iostream>
using namespace std;
class JakobianTablice
{
public:
	double ksi[4];
	double eta[4];
	double N[4][4];

	double ksiB[4][2];
	double etaB[4][2];
	double NB[4][2][4];

	double dN_dksi[4][4];
	double dN_deta[4][4];

	JakobianTablice();

	void wypisz();
	~JakobianTablice();
};

