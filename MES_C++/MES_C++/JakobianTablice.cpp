#include "stdafx.h"
#include "JakobianTablice.h"


JakobianTablice::JakobianTablice()
{
	ksi[0] = -1 / sqrt(3);
	ksi[1] = -ksi[0];

	ksiB[0][0] = ksi[0];
	ksiB[0][1] = -ksi[0];
	etaB[0][0] = -1;
	etaB[0][1] = -1;

	ksiB[1][0] = 1;
	ksiB[1][1] = 1;
	etaB[1][0] = ksi[0];
	etaB[1][1] = ksi[1];

	ksiB[2][0] = ksi[1];
	ksiB[2][1] = ksi[0];
	etaB[2][0] = 1;
	etaB[2][1] = 1;

	ksiB[3][0] = -1;
	ksiB[3][1] = -1;
	etaB[3][0] = ksi[1];
	etaB[3][1] = ksi[0];

	ksi[2] = ksi[1];
	ksi[3] = ksi[0];
	eta[0] = ksi[0];
	eta[1] = eta[0];
	eta[2] = -eta[1];
	eta[3] = eta[2];
	for (int i = 0; i < 4; i++) {
		N[i][0] = 0.25*(1 - ksi[i])*(1 - eta[i]);
		N[i][1] = 0.25*(1 + ksi[i])*(1 - eta[i]);
		N[i][2] = 0.25*(1 + ksi[i])*(1 + eta[i]);
		N[i][3] = 0.25*(1 - ksi[i])*(1 + eta[i]);
//-----------------------------------------------
		dN_dksi[0][i] = -0.25*(1 - eta[i]);
		dN_dksi[1][i] = 0.25*(1 - eta[i]);
		dN_dksi[2][i] = 0.25*(1 + eta[i]);
		dN_dksi[3][i] = -0.25*(1 + eta[i]);
//-----------------------------------------------
		dN_deta[0][i] = -0.25*(1 - ksi[i]);
		dN_deta[1][i] = -0.25*(1 + ksi[i]);
		dN_deta[2][i] = 0.25*(1 + ksi[i]);
		dN_deta[3][i] = 0.25*(1 - ksi[i]);
//-----------------------------------------------
		for (int j = 0; j < 2; j++)
		{
			NB[i][j][0] = 0.25*(1 - ksiB[i][j])*(1 - etaB[i][j]);
			NB[i][j][1] = 0.25*(1 + ksiB[i][j])*(1 - etaB[i][j]);
			NB[i][j][2] = 0.25*(1 + ksiB[i][j])*(1 + etaB[i][j]);
			NB[i][j][3] = 0.25*(1 - ksiB[i][j])*(1 + etaB[i][j]);
		}
	}
}

void JakobianTablice::wypisz()
{
	cout << "ksi:" << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << ksi[i]<<"  ";
	}
	cout <<endl<< "eta:" << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << eta[i] << "  ";
	}
	cout << endl << "N:" << endl;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << N[i][j] << "  ";
		}
		cout << endl;
	}
	cout << endl << "dN_dksi:" << endl;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << dN_dksi[i][j] << "  ";
		}
		cout << endl;
	}
	cout << endl << "dN_deta:" << endl;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << dN_deta[i][j] << "  ";
		}
		cout << endl;
	}
	cout << endl << "NB:" << endl;
	for (int i = 0; i < 4; i++)
	{
		for (int k = 0; k < 2; k++)
		{
			for (int j = 0; j < 4; j++)
			{
			cout << NB[i][k][j] << "  ";
			}
			cout << endl;
		}
		
		cout << endl << endl;
	}
}


JakobianTablice::~JakobianTablice()
{
}
