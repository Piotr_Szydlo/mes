#include "stdafx.h"
#include "Grid.h"


void Grid::GridCreate(double temperatura)
{
	
	double deltaY = H / nH;
	double deltaX = L / nL;
	cout << "Delta Y " << deltaY << " Delta X " << deltaX << endl;
	T = temperatura;
	for (int i = 0; i<nL; i++)
	{
		for (int j = 0; j<nH; j++)
		{
			bool nb;
			if (j == 0 || i == 0 || j == nL - 1 || i == nL - 1)
				nb = true;
			else 
				nb = false;

			Node *NewNode=new Node (i*(L/(1.0*(nL-1))), j*(H/(1.0 *(nH-1))), temperatura,nb);
			node[i*nH + j] = *NewNode;
			if ((i<nL - 1) && (j<nH - 1))
			{
				Element NewElement ( i*nH + j, i*nH + j + nH, i*nH + nH + 1 + j, i*nH + j + 1);
				elements[i*(nH-1) + j] = NewElement;
			}
		}
	}	
}

void Grid::wypisz()
{
	cout << "nL " << nL << " nH " << nH << " H " << H << " L " << L << endl;
	
		for (int j = 0; j < (nH - 1)*(nL-1); j++)
		{
			cout << "Element nr: " << j << "\nZ ID:" << elements[j].id[0] << " " << "Z ID:" << elements[j].id[1] << " "
				<< "Z ID:" << elements[j].id[2] << " " << "Z ID:" << elements[j].id[3] << "\n"
				<< " X: " << node[elements[j].id[0]].x << " Y: " << node[elements[j].id[0]].y
				<< " X: " << node[elements[j].id[1]].x << " Y: " << node[elements[j].id[1]].y
				<< " X: " << node[elements[j].id[2]].x << " Y: " << node[elements[j].id[2]].y
				<< " X: " << node[elements[j].id[3]].x << " Y: " << node[elements[j].id[3]].y<<"\n";

		}
		for (int i = 0; i < nH; i++)
		{
			for (int j = 0; j < nL; j++)
			{
				cout << node[i*nH + j].bc << " ";
			}
			cout << endl;
		}
}

void Grid::calculateSteps(double ambient_temperature, double alfa, double specific_heat, double conductivity, double density,double simulation_step_time,double simulation_time)
{

	JakobianTablice *Jtab = new JakobianTablice();

	for (int i = 0; i < nE; i++)
	{
		int *boards = board(node[elements[i].id[0]], node[elements[i].id[1]], node[elements[i].id[2]], node[elements[i].id[3]]);
		Jakobian2D *J2D = new Jakobian2D(Jtab, getXforElement(elements[i]), getYforElement(elements[i]));
		Matrix_H *matrix_H = new Matrix_H(J2D, Jtab, conductivity, alfa, boards);
		Matrix_C *matrix_C = new Matrix_C(J2D, Jtab, density, specific_heat);
		Wektor_P *wektor_P = new Wektor_P(J2D, Jtab, alfa, ambient_temperature, boards);

		for (int j = 0; j < 4; j++)
		{
			for (int k = 0; k < 4; k++)
			{
				Global_H[elements[i].id[j]][elements[i].id[k]] += matrix_H->H[j][k];
				Global_C[elements[i].id[j]][elements[i].id[k]] += matrix_C->C[j][k];

			}
			Vector_P[elements[i].id[j]] += wektor_P->P[j];
		}
	}

	setGlobalHP(simulation_step_time);
	cout << "Step 0" << endl;
	for (int time = 0; time < simulation_time; time += simulation_step_time)
	{
		cout << "Step after " <<time+ simulation_step_time<< endl;
		Gauss(nw);
		updateP(simulation_step_time);
		cout << "max: " << maxT() << " min: " << minT()<<endl;
	}	
}


int * Grid::board(Node  a, Node  b, Node  c, Node  d)
{
	int *boards = new int[4];
	if (a.bc == true && b.bc == true)
		boards[0] = 1;
	else
		boards[0] = 0;

	if (b.bc == true && c.bc == true)
		boards[1] = 1;
	else
		boards[1] = 0;

	if (c.bc == true && d.bc == true)
		boards[2] = 1;
	else
		boards[2] = 0;

	if (d.bc == true && a.bc == true)
		boards[3] = 1;
	else
		boards[3] = 0;
	
	return boards;
}

double * Grid::getXforElement(Element e)
{
	double *x = new double[4];
	for (int i = 0; i < 4; i++)
	{
		x[i] = node[e.id[i]].x;
	}
	return x;
}

double * Grid::getYforElement(Element e)
{
	double *y = new double[4];
	for (int i = 0; i < 4; i++)
	{
		y[i] = node[e.id[i]].y;
	}
	return y;
}

void Grid::setGlobalHP(double simulation_step_time)
{
	for (int j = 0; j < nw; j++)
	{
		double tempP = 0;
		for (int k = 0; k < nw; k++)
		{
			Global_H[j][k] += (Global_C[j][k] / simulation_step_time);
			tempP += (node[k].t * Global_C[j][k] / simulation_step_time);

		}
		Global_P[j] = -Vector_P[j] + tempP;
	}

	GlobalHP = new double *[nw];
	for (int i = 0; i < nw; i++)
	{
		GlobalHP[i] = new double[nw+1];
	}
	for (int i = 0; i < nw; i++)
	{
		for (int j = 0; j < nw + 1; j++)
		{
			if (j == nw)
			{
				GlobalHP[i][j] = Global_P[i];
			}
			else
				GlobalHP[i][j] = Global_H[i][j];
		}
	}
}

void Grid::updateP(double simulation_step_time)
{
	for (int j = 0; j < nw; j++)
	{
		double tempP = 0;
		for (int k = 0; k < nw; k++)
		{
			tempP += (node[k].t * Global_C[j][k] / simulation_step_time);

		}
		Global_P[j] = -Vector_P[j] + tempP;
		//cout << Global_P[j] << " ";
	}
	//cout << endl;

	for (int i = 0; i < nw; i++)
	{
		for (int j = 0; j < nw + 1; j++)
		{
			if (j == nw)
			{
				GlobalHP[i][j] = Global_P[i];
			}
			else
				GlobalHP[i][j] = Global_H[i][j];
		}
	}
}

void Grid::print()
{
	cout << "Matrix H" << endl;
	for (int i = 0; i < nw; i++)
	{
		for (int j = 0; j < nw; j++)
		{
			cout << Global_H[i][j] << " ";
		}
		cout << endl;
	}

	cout << "Vector P" << endl;

	for (int j = 0; j < nw; j++)
	{
		cout << Global_P[j] << " ";
	}
	cout << endl;
}

void Grid::printTemp()
{
	for (int i = 0; i < nL; i++)
	{
		for (int j = 0; j < nH; j++)
		{
			cout << node[i*nH + j].t << " ";
		}
		cout << endl;
	}
}

double Grid::maxT()
{
	double max = node[0].t;
	for (int i = 1; i < nw; i++)
		if (node[i].t > max)
			max = node[i].t;
	return max;
}

double Grid::minT()
{
	double min = node[0].t;
	for (int i = 1; i < nw; i++)
		if (node[i].t <min)
			min = node[i].t;
	return min;
}

bool Grid::Gauss(int n)
{
	//https://eduinf.waw.pl/inf/alg/001_search/0076.php
	
		const double eps = 1e-12;
		int i, j, k;
		double m, s;

		// eliminacja wsp�czynnik�w

		for (i = 0; i < n - 1; i++) {
			for (j = i + 1; j < n; j++) {
				if (fabs(GlobalHP[i][i]) < eps) return false;
				m = -GlobalHP[j][i] / GlobalHP[i][i];
				for (k = i + 1; k <= n; k++)
					GlobalHP[j][k] += m * GlobalHP[i][k];
			}
		}

		// wyliczanie niewiadomych

		for (i = n - 1; i >= 0; i--) {
			s = GlobalHP[i][n];
			for (j = n - 1; j >= i + 1; j--)
				s -= GlobalHP[i][j] * node[j].t;
			if (fabs(GlobalHP[i][i]) < eps) return false;
			node[i].t = s / GlobalHP[i][i];
		}
		return true;

	

}

Grid::Grid(double H,double L,int nH,int nL)
{
	this->nH = nH;
	this->nL = nL;
	this->H = H;
	this->L = L;
	this->nE = (nH - 1)*(nL - 1);
	node = new Node[nH*nL];
	elements = new Element[(nH - 1)*(nL - 1)];

	nw = nH * nL;
	Global_H = new double *[nw];
	Global_C = new double *[nw];
	Global_P = new double[nw];
	Vector_P = new double[nw];
	for (int i = 0; i < nw; i++)
	{
		Global_H[i] = new double[nw];
		Global_C[i] = new double[nw];

	}
	for (int i = 0; i < nw; i++)
	{
		for (int j = 0; j < nw; j++)
		{
			Global_H[i][j] = 0;
			Global_C[i][j] = 0;
		}
		Global_P[i] = 0;
		Vector_P[i] = 0;
	}
}


Grid::~Grid()
{
}
