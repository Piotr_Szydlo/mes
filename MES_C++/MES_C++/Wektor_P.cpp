#include "stdafx.h"
#include "Wektor_P.h"





Wektor_P::Wektor_P(Jakobian2D * J2D, JakobianTablice * Jtab, double alfa, double ambient_temperature, int * border)
{
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
			{
				if (border[i] == 1)
				{
					P[i]+= (-( Jtab->NB[i][0][j] +Jtab->NB[i][1][j] )*(J2D->len(i) / 2)*alfa*ambient_temperature);
				}
			}
}

Wektor_P::~Wektor_P()
{
}
