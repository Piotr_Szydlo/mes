#pragma once
#include"JakobianTablice.h"
class Jakobian2D
{	double *x;
	double *y;
	double J[4][4];

public:
	double Xp[4];
	double Yp[4];
	double DetJ[4];
	double Jakobian[4][4];

	Jakobian2D(JakobianTablice *Jtab,double X[],double Y[]);
	double len(int a);
	void wypisz();
	~Jakobian2D();
};

