#include "stdafx.h"
#include "Matrix_C.h"




Matrix_C::Matrix_C(Jakobian2D *J2D, JakobianTablice *Jtab, double density, double specific_heat)
{

	for (int k = 0; k<4; k++)
		for (int i = 0; i<4; i++)
			for (int j = 0; j < 4; j++)
			{
				temp[k][i][j] = J2D->DetJ[k] * Jtab->N[k][i] * Jtab->N[j][k] * specific_heat*density;
			}

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			C[i][j] = temp[0][i][j] + temp[1][i][j] + temp[2][i][j] + temp[3][i][j];
		}
	}
	
}

void Matrix_C::wypisz()
{
	cout << "C:" << endl;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << C[i][j] << "  ";
		}
		cout << endl;
	}
}

Matrix_C::~Matrix_C()
{
}
